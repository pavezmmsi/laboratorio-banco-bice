describe('Controllers', function(){ //Descripcion de la Prueba
    beforeEach(module('app.controllers')); //Llamada al Modulo
    describe('Inicio',function(){ //Descripcion del Controller
        var myctrl;
        var $scope = {};
        beforeEach(inject(function($controller){ 
            myctrl = $controller('Inicio',{ $scope: $scope });
        }));
        it('Todo OK ', function(){ 
            expect($scope.names[0].name).toBe('Cobre'); //pass
            expect($scope.names[1].name).toBe('Dolar'); //pass
        });
    });
});