angular.module('app.controllers', ['app.services']).run(function ($rootScope) {

})
   // Arreglo con los Indices a consultar
  .controller("Inicio", function ($scope, getIndices) {
    $scope.names = [{
      'url': '/cobre',
      'name': 'Cobre'
    }, {
      'url': '/dolar',
      'name': 'Dolar'
    }, {
      'url': '/euro',
      'name': 'Euro'
    }, {
      'url': '/ipc',
      'name': 'IPC'
    },
      , {
      'url': '/ivp',
      'name': 'IVP'
    }, {
      'url': '/oro',
      'name': 'Oro'
    }, {
      'url': '/plata',
      'name': 'Plata'
    }, {
      'url': '/utm',
      'name': 'Utm'
    },
    {
      'url': '/yen',
      'name': 'Yen'
    }
    ];
  })
 //Llamada para consultar los Datos del Dolar
  .controller("Dolar", function ($scope, getIndices) {
    getIndices.async().then(function (datos) {
      $scope.titulo = angular.uppercase(datos.dolar.key);
      $scope.subtitulo = datos.dolar.name;
      $scope.value = datos.dolar.value;
    });
  })
  //Llamada para consultar los Datos del Cobre
  .controller("Cobre", function ($scope, getIndices) {
    getIndices.async().then(function (datos) {
      $scope.titulo = angular.uppercase(datos.cobre.key);
      $scope.subtitulo = datos.cobre.name;
      $scope.value = datos.cobre.value;
    });
  })
  //Llamada para consultar los Datos del Euro
  .controller("Euro", function ($scope, getIndices) {
    getIndices.async().then(function (datos) {
      $scope.titulo = angular.uppercase(datos.euro.key);
      $scope.subtitulo = datos.euro.name;
      $scope.value = datos.euro.value;
    });
  })
  //Llamada para consultar los Datos del IPC
  .controller("Ipc", function ($scope, getIndices) {
    getIndices.async().then(function (datos) {
      $scope.titulo = angular.uppercase(datos.ipc.key);
      $scope.subtitulo = datos.ipc.name;
      $scope.value = datos.ipc.value;
    });
  })
  //Llamada para consultar los Datos del IPV
  .controller("Ivp", function ($scope, getIndices) {
    getIndices.async().then(function (datos) {
      $scope.titulo = angular.uppercase(datos.ivp.key);
      $scope.subtitulo = datos.ivp.name;
      $scope.value = datos.ivp.value;
    });
  })
  //Llamada para consultar los Datos del Oro
  .controller("Oro", function ($scope, getIndices) {
    getIndices.async().then(function (datos) {
      $scope.titulo = angular.uppercase(datos.oro.key);
      $scope.subtitulo = datos.oro.name;
      $scope.value = datos.oro.value;
    });
  })
   //Llamada para consultar los Datos de Plata
  .controller("Plata", function ($scope, getIndices) {
    getIndices.async().then(function (datos) {
      $scope.titulo = angular.uppercase(datos.plata.key);
      $scope.subtitulo = datos.plata.name;
      $scope.value = datos.plata.value;
    });
  })
   //Llamada para consultar los Datos de la Dolar
  .controller("Utm", function ($scope, getIndices) {
    getIndices.async().then(function (datos) {
      $scope.titulo = angular.uppercase(datos.utm.key);
      $scope.subtitulo = datos.utm.name;
      $scope.value = datos.utm.value;
    });
  })
   //Llamada para consultar los Datos del YEN
  .controller("Yen", function ($scope, getIndices) {
    getIndices.async().then(function (datos) {
      $scope.titulo = angular.uppercase(datos.yen.key);
      $scope.subtitulo = datos.yen.name;
      $scope.value = datos.yen.value;
    });
  })