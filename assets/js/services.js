angular.module('app.services', ['app.services'])

   .constant("myConfig", {
        //"url": "http://104.154.38.237",
        "url": "http://localhost",
        "port": "4000"
    })    
    .factory('getIndices', function ($http, $rootScope, myConfig) {
        var getIndices = {
          async: function (mmm) {
    
    
            var promise = $http.get('https://www.indecon.online/last', { timeout: 3000 }).then(function (response) {
              return response.data;
            }, function (rejected) {
              return 0;
            });
    
            return promise;
          }
        };
        return getIndices;
      })