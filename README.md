# README #

Se deben seguir las siguientes instrucciones para poner en marcha la aplicacion

### Pre - Instalacion ###

* Instalar Node.js v12.7
* Instalar Git v2.29.2.2
* npm v6.10.0


### Instalacion ###

Se deben seguir los siguientes pasos

* Crear directorio de Trabajo
* Ingresar al directorio
* Ejecutar : git clone https://pavezmmsi@bitbucket.org/pavezmmsi/laboratorio-banco-bice.git
* cd laboratorio-banco-bice
* npm install
* node app.js
* ingresar a cualquier browser a la direccion http://localhost:3000/


### Contexto ###

* Este desarollo esta hecho como parte del proceso de evaluacion de Banco Bice
* Version 1.0
* [Leonardo Pavez](https://pavezmmsi@bitbucket.org/pavezmmsi/laboratorio-banco-bice.git)

### Informacion del Desarrollo ###

Se ocuparon las siguientes tecnologias.

* Desarrollo en Node.js
* Desarrollo Angular.js
* Bootstrap 
* [Endpoint](https://www.indecon.online/last)


