var express = require('express');
var path = require('path');

var app = express();


app.use('/assets',express.static(__dirname + '/assets'));

//llamada index
app.get('/', function(request,response){
	response.sendFile('index.html',{root:path.join(__dirname,'./views')});
});

//llamada indice Cobre
app.get('/cobre', function(request,response){
	response.sendFile('cobre.html',{root:path.join(__dirname,'./views')});
});

//llamada indice Dolar
app.get('/dolar', function(request,response){
	response.sendFile('dolar.html',{root:path.join(__dirname,'./views')});
});
//llamada indice IPC
app.get('/ipc', function(request,response){
	response.sendFile('ipc.html',{root:path.join(__dirname,'./views')});
});
//llamada indice IVP
app.get('/ivp', function(request,response){
	response.sendFile('ivp.html',{root:path.join(__dirname,'./views')});
});
//llamada indice Oro
app.get('/oro', function(request,response){
	response.sendFile('oro.html',{root:path.join(__dirname,'./views')});
});
//for /contact page
app.get('/euro', function(request,response){
	response.sendFile('euro.html',{root:path.join(__dirname,'./views')});
});
//llamada indice Plata
app.get('/plata', function(request,response){
	response.sendFile('plata.html',{root:path.join(__dirname,'./views')});
});
//llamada indice UTM
app.get('/utm', function(request,response){
	response.sendFile('utm.html',{root:path.join(__dirname,'./views')});
});
//llamada indice yen
app.get('/yen', function(request,response){
	response.sendFile('yen.html',{root:path.join(__dirname,'./views')});
});

app.listen(3000,function(){
	console.log('Ingrese a http://localhost:3000');
});

module.exports = app;